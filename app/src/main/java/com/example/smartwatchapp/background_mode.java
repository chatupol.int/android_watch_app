package com.example.smartwatchapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;


public class background_mode extends Service implements SensorEventListener{

    private JSONArray accelerometer_data,heart_rate_data,all;
    private JSONObject json_obj;
    private SensorManager sensorManager;
    private Handler handler;
    private Context context;
    private Intent bg_intent;
    private int bg_flags,bg_startId;
    private BroadcastReceiver restart_receiver;
    private PowerManager.WakeLock wl;


    //my class
    private smartWatch_httpRequest smw_request;
    private notification ntf;

    public background_mode(){
        context=this;
        accelerometer_data=new JSONArray();
        heart_rate_data=new JSONArray();
        all=new JSONArray();
        smw_request=new smartWatch_httpRequest();

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                every_second_status();
                handler.postDelayed(this, 60000);
            }
        }, 60000);
    }

    private void every_second_status() {
        JSONArray accelerometer_static = accelerometer_data, heart_rate_static = heart_rate_data,new_all=all;
        accelerometer_data = new JSONArray();
        heart_rate_data = new JSONArray();
        all=new JSONArray();
        smw_request.save_background_task(new_all);


        //posture
        byte[] x =new byte[accelerometer_static.length()];
        byte[] y =new byte[accelerometer_static.length()];
        byte[] z =new byte[accelerometer_static.length()];
        for(int i=0;i<accelerometer_static.length();i++){
            try {
                JSONObject item=accelerometer_static.getJSONObject(i);
                x[i]=Byte.parseByte(item.getString("x"));
                y[i]=Byte.parseByte(item.getString("y"));
                z[i]=Byte.parseByte(item.getString("z"));
            } catch (JSONException e) {}
        }

        boolean x_result,y_result,z_result;
        float avg=find_average(x);
        float max=find_max_float(x);
        float result=max-avg;

        x_result=(result>30)?true:false;

        avg=find_average(y);
        max=find_max_float(y);
        result=max-avg;
        y_result=(result>30)?true:false;

        avg=find_average(z);
        max=find_max_float(z);
        result=max-avg;
        z_result=(result>30)?true:false;
        if(y_result&&(x_result||z_result)){
//            smw_request.save_posture("u0001","have falling");
        }else{
//            smw_request.save_posture("u0001","stay");
        }
    }

    private float find_average(byte[] reals){
        float result,sum=0;
        for(int i=0;i<reals.length;i++){
            sum+=reals[i];
        }
        result=(sum/reals.length);
        return result;
    }

    private byte find_max_float(byte[] reals){
        byte result=0;
        for(int i=0;i<reals.length;i++){
            if(reals[i]>result){
                result=reals[i];
            }
        }
        return result;
    }

    private void sensor_listener(){
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onCreate() {
        restart_receiver  = new Restarter();
        context=this.getBaseContext();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);//หากยังไม่ถอดสาย usb ตอน debug ก็จะสามารถใช้งาน background service แต่หากถอดสายแล้ว background จะไม่ทำงาน ต้องใชโค้ดนี้
        wl =pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp::mywakelocktag");//และก็อันนี้ด้วย เพื่อบอกให้มันตื่นตลอดเวลา

        ntf=new notification(context);
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_ANSWER);
        registerReceiver(new Restarter(), filter);
        this.sensor_listener();
        Log.d("tiger","listener start...");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        wl.acquire();
        onTaskRemoved(intent);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        ntf.send_custom_content_notification("crash","background_mode.OnDestroy() exceuted...");
        Log.d("tiger_remove","background_mode.onTaskRemove()");
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        Log.d("tiger_remove","background_mode.onDestroy()");
        ntf.send_custom_content_notification("crash","background_mode.OnDestroy() exceuted...");
        try{
            unregisterReceiver(restart_receiver);
        }catch (Exception ex){}
        Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        json_obj=new JSONObject();
        try {
            json_obj.put("t",timestamp+"");
        } catch (JSONException e) {
            Log.d("save error",e.getMessage());
        }
        switch (sensorEvent.sensor.getName()) {
            case "ACCELEROMETER" :
                String result="";
                for(int i=0;i<sensorEvent.values.length;i++){
                    result+=sensorEvent.values[i]+" ";
                }
                try {
                    json_obj.put("v",result);
                    json_obj.put("n","ac");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                accelerometer_data.put(json_obj);break;
            case "HEART_RATE":
                try {
                    json_obj.put("v",((int)Math.ceil(sensorEvent.values[0]))+"");
                    json_obj.put("n","hr");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                heart_rate_data.put(json_obj);;break;
            default:;
        }
        all.put(json_obj);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}
}
