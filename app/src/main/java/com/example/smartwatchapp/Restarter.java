package com.example.smartwatchapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Restarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, background_mode.class);
        if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
            Log.d("tiger","1");
            i.setAction(Intent.ACTION_SCREEN_OFF);
        }else if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("tiger","2");
            i.setAction(Intent.ACTION_SCREEN_ON);
        }else{
            Log.d("tiger","3");
            i.setAction(Intent.ACTION_SCREEN_OFF);
        }
        context.startService(i);
    }
}
