package com.example.smartwatchapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;

import androidx.core.app.NotificationCompat;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class smartWatch_httpRequest {

//    private String url="http://192.168.1.170:3000/";
//    private String url="http://183.88.230.105:3001/";
    private String url="http://192.168.1.33:3001/";

    smartWatch_httpRequest(){

    }

    public void save_background_task(JSONArray data){
        if(data.toString().length()>2) {
            Log.d("tiger_save_bg", "background");
            Log.d("tiger_save_bg",data.toString().length()+" char");
            String path = "save_data/all";
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), data.toString());
            OkHttpClient client = new OkHttpClient();

            Request request;
            request = new Request.Builder()
                    .url(url + path)
                    .post(body)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                }
            });
        }else{
            Log.d("tiger_save_bg", "empty");
        }
    }

    public void save_posture(String u_id,String posture){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String path="save_data/watch_wearer";
        JSONArray records=new JSONArray();
        JSONObject record=new JSONObject();
        try {
            record.put("u_id",u_id);
            record.put("posture",posture);
            record.put("timestamp",timestamp+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        records.put(record);
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),records.toString());
        OkHttpClient client = new OkHttpClient();

        Request request;
        request =new Request.Builder()
                .url(url+path)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
            }
        });
    }

    public void falling_alert(String u_id,String moderator_id){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String path="falling";
        JSONObject record=new JSONObject();
        try {
            record.put("u_id",u_id);
            record.put("moderator_id",moderator_id);
            record.put("timestamp",timestamp+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),record.toString());
        OkHttpClient client = new OkHttpClient();

        Request request;
        request =new Request.Builder()
                    .url(url+path)
                    .post(body)
                    .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){

                }
            }
        });
    }
}
