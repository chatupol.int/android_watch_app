package com.example.smartwatchapp;

import android.util.Log;

import org.json.JSONArray;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class httpRequest {

    private String url="http://192.168.1.170:3000/save_data/";
    String result,http_method;
    JSONArray json_array;

    httpRequest(String httpMethod, JSONArray jsonArray,String db){
        http_method=httpMethod;
        json_array=jsonArray;
        url=url+db;
    }

    public String getResult(){
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),this.json_array.toString());
        OkHttpClient client = new OkHttpClient();

        Request request;
        switch (this.http_method){
            case "POST":request =new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();break;
            default:request =new Request.Builder()
                    .url(url)
                    .build();break;
        }

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    result =response.body().string();
                }
            }
        });
        return this.result+"test";
    }
}
