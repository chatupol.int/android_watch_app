package com.example.smartwatchapp;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;


public class background_worker extends Worker  {

    Context context;
    notification ntf;
    public background_worker(@NonNull Context ct, @NonNull WorkerParameters workerParams) {
        super(ct, workerParams);
        context=ct;
        ntf=new notification(ct);
    }

    @Override
    public Result doWork() {
        Log.d("tiger","background start");
        ntf.send_ez_notify();
        new sensorListener(context);
        return Result.success();
    }
}
