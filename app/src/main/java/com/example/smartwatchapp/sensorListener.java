package com.example.smartwatchapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.Math;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.SENSOR_SERVICE;

public class sensorListener  implements SensorEventListener {

    private SensorManager sensorManager;
    private Context context;
    JSONArray result = new JSONArray();
    JSONObject item = new JSONObject();
    private FusedLocationProviderClient mFusedLocationClient;
    private smartWatch_httpRequest swr;
    private String location_result="";
    private notification ntf;

    public sensorListener(Context ct){
        Log.d("tiger","sensorListener start...");
        swr=new smartWatch_httpRequest();
        ntf=new notification(ct);
        this.context=ct;
        this.sensor_listener();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(ct);
        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location==null){
                    location_result="0";
                }else{
                    location_result=location.getLatitude()+","+location.getLongitude();
                }
            }
        });


        if(result.toString().length()!=2){
            swr.save_background_task(result);
        }else{
            ntf.send_custom_content_notification("Save to db status...","sensors data is empty.");
        }
    }

    private void sensor_listener(){
        sensorManager=(SensorManager)context.getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        item=new JSONObject();
        float[] values=new float[sensorEvent.values.length];
        String str_values="";
        for(int i=0;i<sensorEvent.values.length;i++){
            values[i]=sensorEvent.values[i];
            str_values+=sensorEvent.values[i]+" ";
        }
        switch (sensorEvent.sensor.getName()) {
            case "ACCELEROMETER" :
                if(Math.abs(values[0])>25||Math.abs(values[1])>25||Math.abs(values[2])>25){
                    sensorManager.unregisterListener(this);
                    swr.falling_alert("u0001","m0001");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.sensor_listener();
                }
                break;
            case "HEART_RATE":
                break;
            case "STEP_COUNTER":break;
            default:;
        }

        try{
            item.put("location",location_result);
            item.put("sensor_values",str_values);
            item.put("sensor_name",sensorEvent.sensor.getName());
            item.put("timestamp",timestamp+"");
        }catch (Exception ex){}
//        Log.d("sensor_result",result.toString());
        result.put(item);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
