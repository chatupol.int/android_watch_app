package com.example.smartwatchapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import androidx.core.app.NotificationCompat;

public class notification {

    private Notification notification;
    private NotificationManager notificationManager;
    private Context context;

    notification(Context ct){
        this.context=ct;
        notificationManager =(NotificationManager) ct.getSystemService(Context.NOTIFICATION_SERVICE);

    }

    public void send_ez_notify(){
        notification = new NotificationCompat.Builder(context, "ggez")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("test")
                .setContentText("background running...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(false)
                .build();
        notificationManager.notify(1000, notification);
    }

    public void send_custom_content_notification(String title,String content){
        notification = new NotificationCompat.Builder(context, "ggez")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(false)
                .build();
        notificationManager.notify(1000, notification);
    }
}

