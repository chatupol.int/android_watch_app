package com.example.smartwatchapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.KeyEventDispatcher;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import static android.app.AlarmManager.ELAPSED_REALTIME;
import static android.os.SystemClock.elapsedRealtime;

//10.37 cpuz start

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView showHeartRate,accelerometer,step_counter,myText_test;
    private SensorManager sensorManager;
    private int step_increment=0,db_index=0;
    private int BODY_SENSORS_PERMISSION_CODE=1,ACCESS_COARSE_LOCATION=1,ACCESS_FINE_LOCATION=1;
    private FusedLocationProviderClient mFusedLocationClient;
    private Button btn,rm_btn,start_btn,stop_btn,switch_btn;
    private String[] switch_db;
    private int[] switch_color;
    private JSONObject sensor_data;
    private JSONArray sensors_data;
    private Context ct;

    //force lock screen
    private DevicePolicyManager devicePolicyManager;
    private ActivityManager activityManager;
    private ComponentName componentName;
    private final int RESULT_ENABLE=11;

    //my class
    private smartWatch_httpRequest swr;
    private background_mode bm;
    BroadcastReceiver restart_receiver ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("tiger_event","MainActiviy.onCreate()");
        setContentView(R.layout.activity_main);

        swr=new smartWatch_httpRequest();
        ct=this;

        restart_receiver  = new Restarter();
        foreground_register_receiver();
        devicePolicyManager=(DevicePolicyManager)getSystemService(DEVICE_POLICY_SERVICE);
        activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        componentName=new ComponentName(this,MyAdmin.class);

        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        showHeartRate=(TextView) findViewById(R.id.heartRateText);
        accelerometer=(TextView)findViewById(R.id.accelerometer);
        step_counter=(TextView)findViewById(R.id.step_counter1);
        myText_test=(TextView) findViewById(R.id.myText);
        myText_test.setVisibility(View.INVISIBLE);
        btn=(Button)findViewById(R.id.button);
        rm_btn=(Button)findViewById(R.id.rm);
        start_btn=(Button)(findViewById(R.id.start));
        stop_btn=(Button)(findViewById(R.id.stop));

        switch_btn=(Button)findViewById(R.id.db_switch);
        switch_db=new String[]{"all","falling","walk"};
        switch_color=new int[]{Color.YELLOW,Color.RED,Color.GREEN};
        switch_btn.setText(switch_db[db_index]);
        switch_btn.setBackgroundColor(switch_color[db_index]);
        switch_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db_index=(db_index+1)%switch_db.length;
                switch_btn.setText(switch_db[db_index]);
                switch_btn.setBackgroundColor(switch_color[db_index]);
            }
        });

        sensors_data=new JSONArray();
        sensor_data=new JSONObject();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new httpRequest("POST",sensors_data,switch_db[db_index]).getResult();
//                sensors_data=new JSONArray();

                //minimizeApp();
//                Intent in=new Intent();
//                sendBroadcast(in);

                boolean active =devicePolicyManager.isAdminActive(componentName);
                if(active){
                    devicePolicyManager.lockNow();
                }else{
                    Log.d("tiger_lock","else in btn");
                    Intent intent=new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,componentName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,"Additional text explaining why we need this permission.");
                    startActivityForResult(intent,RESULT_ENABLE);
                }
            }
        });

        rm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sensors_data=new JSONArray();
                sensorManager.unregisterListener(MainActivity.this);
                btn.setBackgroundColor(Color.LTGRAY);
                start_btn.setBackgroundColor(Color.LTGRAY);
                stop_btn.setBackgroundColor(Color.LTGRAY);
                rm_btn.setBackgroundColor(Color.RED);
            }
        });
        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn.setBackgroundColor(Color.GREEN);
                start_btn.setBackgroundColor(Color.GREEN);
                stop_btn.setBackgroundColor(Color.LTGRAY);
                sensor_listener();
            }
        });
        stop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn.setBackgroundColor(Color.LTGRAY);
                start_btn.setBackgroundColor(Color.LTGRAY);
                sensorManager.unregisterListener(MainActivity.this);
            }
        });


        showHeartRate.setText("ssss");
        check_permissions();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location==null){
                    myText_test.setText("location==null : "+(location==null));
                }else{
                    myText_test.setText("location==null : "+(location==null)+"\nlat : "+location.getLatitude()+" lon : "+location.getLongitude());
                }
            }
        });
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            myText_test.setText(mLastLocation.getLatitude()+" "+mLastLocation.getLongitude());
        }
    };

    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private void check_permissions(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.BODY_SENSORS)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.BODY_SENSORS)){
                new AlertDialog.Builder(this).
                        setTitle("Permission needed").
                        setMessage("Body sensors permission is needed when you OK ,this app will restart itself.").
                        setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.BODY_SENSORS},BODY_SENSORS_PERMISSION_CODE);
                            }
                        }).
                        setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BODY_SENSORS},BODY_SENSORS_PERMISSION_CODE);
            }
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)){
                new AlertDialog.Builder(this).
                        setTitle("Permission needed").
                        setMessage("Body sensors permission is needed when you OK ,this app will restart itself.").
                        setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},ACCESS_COARSE_LOCATION);
                            }
                        }).
                        setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},this.ACCESS_COARSE_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==BODY_SENSORS_PERMISSION_CODE){
            if(grantResults.length>0&& grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"Permission GRANTED",Toast.LENGTH_LONG).show();
                try {
                    myText_test.setText("app will restart itself...");
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent mStartActivity = new Intent(MainActivity.this, MainActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(MainActivity.this, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)MainActivity.this.getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);
            }else {
                Toast.makeText(this,"Permission DENIED",Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Log.d("tigerSensor",sensorEvent.sensor.getName()+" : "+sensorEvent.values[0]+" "+sensorEvent.values[1]+" "+sensorEvent.values[2]);
//        String result ="";
//        for(int i =0;i<sensorEvent.values.length;i++){
//            result+=sensorEvent.values[i]+" ";
//        }
//        switch (sensorEvent.sensor.getName()) {
//            case "ACCELEROMETER" :
//                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                float[] values=new float[3];
//                for(int i=0;i<sensorEvent.values.length;i++){
//                    values[i]=Math.abs(sensorEvent.values[i]);
//                }
//                if(values[0]>25||values[1]>25||values[2]>25){
//                    sensorManager.unregisterListener(this);
//                    swr.falling_alert("u0001","m0001");
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    this.sensor_listener();
//                }
//                break;
//            case "HEART_RATE":showHeartRate.setText(result);break;
//            case "STEP_COUNTER":step_counter.setText("STEP_COUNTER\n"+step_increment);step_increment++;break;
//            default:myText_test.setText(result);
//        }
//        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//        sensor_data=new JSONObject();
//        try {
//            sensor_data.put("sensor_name",sensorEvent.sensor.getName());
//            sensor_data.put("sensor_values",result);
//            sensor_data.put("timestamp",timestamp+"");
//        } catch (JSONException e) {
//            Log.d("save error",e.getMessage());
//        }
//        sensors_data.put(sensor_data);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    private void sensor_listener(){
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE), SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void foreground_register_receiver(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(restart_receiver,filter);
        Log.d("tiger","foreground_start");
    }

    private void background_task(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        registerReceiver(restart_receiver,filter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("tiger","back btn");
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    @Override
    protected void onResume() {
        super.onResume();
        foreground_register_receiver();

        boolean isActive=devicePolicyManager.isAdminActive(componentName);
        Log.d("tiger_event","MainActiviy.onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(restart_receiver);
//        foreground_register_receiver();

        startService(new Intent(MainActivity.this,background_mode.class));
        Log.d("tiger_event","MainActiviy.onPause()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(restart_receiver);
        startService(new Intent(MainActivity.this,background_mode.class));
        Log.d("tiger_event","MainActiviy.onDestroy()");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("tiger_keyEvent",event.getKeyCode()+" ss");
        if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
            Log.d("tiger_power","click");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case RESULT_ENABLE:
                if(resultCode== Activity.RESULT_OK){
                    Log.d("tiger_lock","ggwp");
                }else{
                    Log.d("tiger_lock","ggez");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}